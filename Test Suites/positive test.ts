<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>positive test</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>7a08635c-3cb0-4187-b4a1-82ef05f1b4fe</testSuiteGuid>
   <testCaseLink>
      <guid>9d0c53e4-ffe8-4886-ab60-ec63da305ea5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/positive/PTS01 - Login/PTC01 - login with valid data</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>a5ad417b-8345-46a3-ad30-b78f8bcf65e1</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/user</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>a5ad417b-8345-46a3-ad30-b78f8bcf65e1</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>92337e6f-8507-444b-a253-82181c39631b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>a5ad417b-8345-46a3-ad30-b78f8bcf65e1</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>1d401358-6eab-4daf-a60a-9af5a24c841e</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>eef98cc9-a160-4fcc-8465-d808e0d65fb7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/positive/PTS02 - Checkout Product/PTC02 - beli product</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>fd89c732-ecb8-4d44-8091-c1c8bfc6cfba</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/user buy product</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>fd89c732-ecb8-4d44-8091-c1c8bfc6cfba</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>c4dc04ba-aad0-49ab-b644-b1de747a38eb</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>fd89c732-ecb8-4d44-8091-c1c8bfc6cfba</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>b13e5a8c-34e4-4f0b-a3d9-dcdc82247b28</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>fd89c732-ecb8-4d44-8091-c1c8bfc6cfba</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>first</value>
         <variableId>6898b3b9-6b78-41f2-92da-403b1f1a4c12</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>fd89c732-ecb8-4d44-8091-c1c8bfc6cfba</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>last</value>
         <variableId>7e3e433f-8d15-46ff-8ff0-8f52973c55f2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>fd89c732-ecb8-4d44-8091-c1c8bfc6cfba</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>pos</value>
         <variableId>2ef671ad-ead2-419e-a0a6-00036d252bec</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
